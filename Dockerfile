FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 8080
ADD target/gateway-service-0.0.1-SNAPSHOT.jar gateway-service.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/gateway-service.jar"]